# Pytest plugin for [docker](https://www.docker.com/) containers use.

Plugin implements some fixtures to run various services, that required during
 testing proces, in docker containers.


## Getting started

### Installation

We have no pypi account yet and we have no private pypi repository.
So, you should install this package as usual with `pip` package manager in 
virtualenv or just globally from source:
```bash
pip install git+https://bitbucket.org/bfg-soft/pytest_docker.git
```
If you want to install specific version or branch, you should execute:
```bash
pip install git+https://bitbucket.org/bfg-soft/pytest_docker.git@v0.1.0
```

*Plugin is available for `pytest` automatically, because of it\`s entry_point.*


### Content

#### Overwriting fixtures in your tests.
While using `pytest_docker` plugin, you can overwrite fixture 
`docker_event_loop` in your project. It must return current 
 `asyncio.AbstractEventLoop` instance for tests. Optionally.
 
Example:
```python
from pytest import fixture


@fixture()
def loop():
    # Some fixture, that is defined in yourpackage tests, or provided from 
    # another plugin (like pytest-asyncio or pytest-aiohttp, for example).
    ...


@fixture()
def docker_event_loop(loop):
    return loop    
```


#### Available fixtures

* `docker_container_map` - returns a `dict` with running containers (key - 
container name, value - `aiodocker.containers.DockerContainer` instance).

* `docker_runner` - returns instance of `pytest_docker.PytestDockerRunner` 
(see it\`s source code).

* `docker_get_random_free_tcp_port` - returns function, that will return free
 available tcp port in your operating system.
 
* `docker_run` - return coroutine function, that runs docker container, 
and accepts:
    * `image_name` as required positional argument,
    * `container_name` as required positional argument,
    * `tcp_port` as keyword `bool` argument (default value is `True`)
    * `wait_coro_fn` as keyword argument, that accepts coroutine function to 
    wait container accessibility. Optionally.
    * other various keyword arguments for container creation via docker API
    (see https://docs.docker.com/engine/api/v1.35/#operation/ContainerCreate)


## License

This project is licensed under the MIT License - see the 
[LICENSE.txt](LICENSE.txt) file for details.
