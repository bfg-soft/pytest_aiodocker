from asyncio import AbstractEventLoop, get_event_loop, iscoroutinefunction
from socket import AF_INET, SOCK_STREAM, socket
from typing import Any, Awaitable, Callable, Iterator, MutableMapping, Optional

from aiodocker.containers import DockerContainer
from pytest import fixture

from .runner import PytestAioDockerRunner

__all__ = [
    'docker_event_loop',
    'docker_session_event_loop',
    'docker_container_map',
    'docker_runner',
    'docker_session_runner',
    'docker_get_random_free_tcp_port',
    'docker_run',
    'docker_session_run',
]


@fixture(scope='session')
def docker_container_map() -> MutableMapping:
    return {}


@fixture()
def docker_event_loop() -> AbstractEventLoop:
    return get_event_loop()


@fixture(scope='session')
def docker_session_event_loop() -> AbstractEventLoop:
    return get_event_loop()


def _make_docker_runner_fixture(
    event_loop: AbstractEventLoop,
    container_map: MutableMapping
) -> Iterator[PytestAioDockerRunner]:

    runner = PytestAioDockerRunner.from_env(container_map)

    try:
        yield runner

    finally:
        event_loop.run_until_complete(runner.close())


@fixture()
def docker_runner(
    docker_event_loop,
    docker_container_map,
) -> PytestAioDockerRunner:
    yield from _make_docker_runner_fixture(docker_event_loop,
                                           docker_container_map)


@fixture(scope='session')
def docker_session_runner(
    docker_session_event_loop,
    docker_container_map,
) -> PytestAioDockerRunner:
    yield from _make_docker_runner_fixture(docker_session_event_loop,
                                           docker_container_map)


@fixture(scope='session')
def docker_get_random_free_tcp_port() -> Callable[[], int]:

    def get():
        with socket(AF_INET, SOCK_STREAM) as s:
            # OS takes free port.
            s.bind(('', 0))
            return s.getsockname()[1]

    return get


class _DockerRunFactory(object):

    def __init__(self,
                 docker_runner: PytestAioDockerRunner,
                 random_port_getter: Callable[[], int]):
        self._docker_runner = docker_runner
        self._random_port_getter = random_port_getter

    async def __call__(
        self,
        image_name: str,
        container_name: str,
        container_port: int,
        tcp_port: bool = True,
        wait_coro_fn: Optional[Callable[[DockerContainer], Awaitable]] = None,
        **run_kwargs: Any
    ) -> int:

        host_port = self._random_port_getter()
        docker_runner = self._docker_runner

        await docker_runner.run(
            container_name,
            image_name,
            container_port,
            host_port,
            tcp_port=tcp_port,
            **run_kwargs,
        )

        if iscoroutinefunction(wait_coro_fn):
            await docker_runner.wait_for(container_name, wait_coro_fn)

        return host_port


@fixture()
def docker_run(docker_get_random_free_tcp_port,
               docker_runner):
    return _DockerRunFactory(docker_runner,
                             docker_get_random_free_tcp_port)


@fixture(scope='session')
def docker_session_run(docker_get_random_free_tcp_port,
                       docker_session_runner):
    return _DockerRunFactory(docker_session_runner,
                             docker_get_random_free_tcp_port)
