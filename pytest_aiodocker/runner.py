from asyncio import iscoroutinefunction, sleep
from contextlib import suppress
from copy import copy, deepcopy
from logging import getLogger
from typing import Awaitable, Callable, List, MutableMapping, Optional, Union

from aiodocker import Docker
from aiodocker.containers import DockerContainer
from aiodocker.exceptions import DockerError

__all__ = [
    'PytestAioDockerRunner',
]


_logger = getLogger(__name__)


def _extract_container_name(container: DockerContainer) -> Optional[str]:
    """Container name extractor."""
    with suppress(KeyError, IndexError, AttributeError):
        return container['Names'][0].strip('/')


async def _supress_error(awaitable: Awaitable,
                         success_message: str,
                         error_message: str) -> None:
    """Executing awaitable with possible error supressing."""
    try:
        await awaitable
        _logger.debug(success_message)

    except DockerError as err:
        _logger.error(error_message)
        _logger.exception(err)


class PytestAioDockerRunner(object):
    """Simple pytest docker container runner."""
    __slots__ = '_client', '_containers'

    def __init__(self, client: Docker, containers: MutableMapping) -> None:
        self._client = client
        self._containers = containers

    @classmethod
    def from_env(cls, containers: MutableMapping) -> 'PytestAioDockerRunner':
        return cls(Docker(), containers)

    @property
    def containers(self) -> MutableMapping:
        return copy(self._containers)

    async def _list_containers(self) -> List[DockerContainer]:
        return await self._client.containers.list(all=True)

    async def get_container(self,
                            container_name: str) -> Optional[DockerContainer]:
        container = self._containers.get(container_name)

        if container is not None:
            return container

        for container in await self._list_containers():
            if _extract_container_name(container) == container_name:
                return container

    async def delete_container(self,
                               container: DockerContainer,
                               container_name: Optional[str] = None) -> None:
        """Container deleting."""
        if container_name is None:
            container_name = _extract_container_name(container)

        if container_name in self._containers:
            del self._containers[container_name]

        # https://docs.docker.com/engine/api/v1.35/#operation/ContainerDelete
        await _supress_error(
            container.delete(v=True, force=True),
            'Container {!r} has been deleted.'.format(container_name),
            'Container {!r} can not be deleted.'.format(container_name),
        )

    async def kill_container(self,
                             container: DockerContainer,
                             delete: bool = False) -> None:
        if container['State'] == 'running':
            container_name = _extract_container_name(container)
            await _supress_error(
                container.kill(),
                'Container {!r} has been killed.'.format(container_name),
                'Container {!r} can not be killed.'.format(container_name),
            )

        if delete:
            await self.delete_container(container)

    async def run(self,
                  container_name: str,
                  image_name: str,
                  container_port: Union[int, str],
                  host_port: Union[int, str],
                  tcp_port: bool = True,
                  **run_kwargs) -> DockerContainer:
        container = await self.get_container(container_name)

        if container is not None:
            await self.kill_container(container, delete=True)

        # For details look at
        # https://docs.docker.com/engine/api/v1.35/#operation/ContainerCreate
        config = deepcopy(run_kwargs)
        config['Image'] = image_name
        host_config = config.setdefault('HostConfig', {})
        host_config['PortBindings'] = {
            '{}/{}'.format(
                container_port,
                tcp_port and 'tcp' or 'udp'
            ): [
                {'HostPort': str(host_port)}
            ]
        }

        self._containers[
            container_name
        ] = container = await self._client.containers.run(
            config,
            name=container_name,
        )
        return container

    async def wait_for(
        self,
        container_name: str,
        wait_coro_fn: Callable[[DockerContainer], Awaitable]
    ) -> None:
        assert iscoroutinefunction(wait_coro_fn)

        container = self._containers.get(container_name)

        if container is None:
            _logger.error(
                'Container {!r} had been already stopped.'.format(
                    container_name
                )
            )
            raise RuntimeError()

        while not await wait_coro_fn(container):
            await sleep(.1)

    async def close(self) -> None:
        for container_name, container in frozenset(self._containers.items()):

            _logger.debug('Deleting {!r} container.'.format(container_name))
            await self.delete_container(
                container,
                container_name=container_name
            )

        await self._client.close()
        _logger.debug('PytestAioDockerRunner has just been stopped.')

    async def __aenter__(self) -> 'PytestAioDockerRunner':
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb) -> None:
        await self.close()
