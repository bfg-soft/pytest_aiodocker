from setuptools import find_packages, setup

_NAME = 'pytest_aiodocker'


setup(
    name=_NAME,
    version='0.3.0',
    packages=find_packages(),
    url='https://bitbucket.org/bfg-soft/pytest_aiodocker',
    license='MIT',
    author='BFG-Soft',
    author_email='info@bfg-soft.ru',
    description='Pytest plugin for using docker containers during testing.',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Plugins',
        'Framework :: Pytest',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Topic :: Software Development :: Testing',
    ],
    install_requires=[
        'pytest',
        'aiodocker',
    ],
    entry_points={
        'pytest11': [
            '{0} = {0}'.format(_NAME)
        ]
    }
)
